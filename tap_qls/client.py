"""REST client handling, including QlsStream base class."""

from pathlib import Path
from typing import Any, Dict, Optional, Iterable

import requests
import copy
import time
from pendulum import parse
from singer_sdk.authenticators import BasicAuthenticator
from singer_sdk.streams import RESTStream
from singer_sdk.helpers.jsonpath import extract_jsonpath
from singer_sdk.exceptions import FatalAPIError, RetriableAPIError
import datetime

SCHEMAS_DIR = Path(__file__).parent / Path("./schemas")


class QlsStream(RESTStream):
    """Qls stream class."""
    extra_retry_statuses = [429,101]
    
    def url_base(self) -> str:
        company_id = self.config["company_id"]
        url = f"https://api.pakketdienstqls.nl/companies/{company_id}"
        return url

    today= None

    @property
    def authenticator(self) -> BasicAuthenticator:
        """Return a new authenticator object."""
        return BasicAuthenticator.create_for_stream(
            self,
            username=self.config.get("username"),
            password=self.config.get("password"),
        )

    @property
    def http_headers(self) -> dict:
        """Return the http headers needed."""
        headers = {}
        if "user_agent" in self.config:
            headers["User-Agent"] = self.config.get("user_agent")
        return headers

    def get_url(self, context: Optional[dict]) -> str:
        """Get stream entity URL.

        Developers override this method to perform dynamic URL generation.

        Args:
            context: Stream partition or context dictionary.

        Returns:
            A URL, optionally targeted to a specific partition or context.
        """
        url = "".join([self.url_base(), self.path or ""])
        vals = copy.copy(dict(self.config))
        vals.update(context or {})
        for k, v in vals.items():
            search_text = "".join(["{", k, "}"])
            if search_text in url:
                url = url.replace(search_text, self._url_encode(v))
        return url

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:
        """Return a token for identifying next page or None if no more pages."""
        if response.status_code not in [404, 204]:
            res_json = response.json()
            if len(res_json["pagination"]) == 0:
                next_page_token = None
            elif res_json["pagination"]["nextPage"]:
               next_page_token = int(res_json["pagination"]["page"]) + 1
            else:
                next_page_token = None
            return next_page_token  

    def get_starting_time(self, context):
        start_date = self.config.get("start_date")
        if start_date:
            start_date = parse(self.config.get("start_date"))
        rep_key = self.get_starting_timestamp(context)
        return rep_key or start_date

    def get_url_params(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Dict[str, Any]:
        """Return a dictionary of values to be used in URL parameterization."""
        params: dict = {}
        if next_page_token:
            params["page"] = next_page_token
        if self.replication_key:
            start_date = self.get_starting_time(context)
            start_date = start_date.strftime('%Y-%m-%dT%H:%M:%S.%f') if start_date else start_date
            params["filter[modified >]"] = start_date
            if not self.today:
                tz = datetime.timezone(datetime.timedelta(hours=2), "GMT+2")
                today = datetime.datetime.now(tz)
                today = today.strftime('%Y-%m-%dT%H:%M:%S.%f')
                self.today = today
            params["filter[modified <]"] = self.today
        self.logger.info(f"Stream {self.name} Endpoint {self.path} Params {params}")
        return params
    
    def validate_response(self, response: requests.Response) -> None:
        if (
            response.status_code in self.extra_retry_statuses
            or 500 <= response.status_code < 600
        ):
            msg = self.response_error_message(response)
            if response.status_code==429 and response.headers.get("retry-after"):
                time.sleep(int(response.headers["retry-after"]))
            raise RetriableAPIError(msg, response)
        elif 400 <= response.status_code < 500 and response.status_code not in [404, 204]:
            msg = self.response_error_message(response)
            raise FatalAPIError(msg)

    def parse_response(self, response: requests.Response) -> Iterable[dict]:
        if response.status_code not in [404, 204]:
            yield from extract_jsonpath(self.records_jsonpath, input=response.json())